package com.epam.cign.pvdr.business;

/**
 * Represents an example API of service that should be intercepted with Aspect.
 * The other examples of similar services are: transactional script, JSP, presentation model.
 *
 * @version 1.0, 04/07/18
 * @since        04/07/18
 */
public interface SimpleService {

  String returnTextAsIs(String text);

}
