package com.epam.cign.pvdr.aop;

/**
 * Represents an example of simple normalization logic called from AOP Advice.
 * The other examples of similar services are: transaction management, logging, security.
 *
 * @version 1.0, 04/07/18
 * @since        04/07/18
 */
public class NormalizationService {

  public Object normalizeData(Object data) {
    if (data instanceof String) {
      return ((String) data).trim().toUpperCase();
    }
    return data;
  }

}
