package com.epam.cign.pvdr.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;


/**
 * This is just an example of Aspect with DI based on pure AspectJ, w/o any IoC.
 *
 * @version 1.0, 04/07/18
 * @since        04/07/18
 */
@Aspect
public class SimpleAspect {

  /**
   * Shared instance to Aspect.
   * It is created only once per application start during class loading.
   */
  private static volatile SimpleAspect instance;

  /** Default implementation for simplicity. */
  private NormalizationService normalizationService = new NormalizationService();

  /** Accessor to aspect instance. Just for example the simple sync. */
  public synchronized static SimpleAspect getInstance() { return instance; }

  /** Setter injection. Could be used with IoC. */
  public synchronized void setNormalizationService(NormalizationService normalizationService) {
    this.normalizationService = normalizationService;
  }

  /**
   * Will be called automatically with AspectJ initialization.
   * For simplicity bypassing the sanity checks that class can be incorrectly initialized multiple
   * times with bad code.
   * @see "META-INF/aop.xml"
   */
  public SimpleAspect() {
    // in production code this checks should be smarter.
    if (getInstance() != null) {
      throw new RuntimeException("Only one instance of aspect should be created with AspectJ");
    }
    instance = this;
  }

  @Pointcut("execution( public java.lang.String com.epam.cign.pvdr.business..*(..) ) && if()")
  public static boolean anyBusinessLogicCallReturnedString() {
    return getInstance().normalizationService != null;
  }

  @Around("anyBusinessLogicCallReturnedString()")
  public Object doNormalizationForStringBusinessServicesResult(ProceedingJoinPoint joinPoint) throws Throwable {
    Object result = joinPoint.proceed(joinPoint.getArgs());
    result = normalizationService.normalizeData(result);
    return result;
  }



}
