package com.epam.cign.pvdr.aop;

import com.epam.cign.pvdr.business.SimpleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

/**
 * Test that {@link NormalizationService} is called on each business service result.
 * It should be done with {@link SimpleAspect} logic.
 *
 * The same approach can be used for other test strategies like:
 * <ul>
 *   <li>Test that all the methods in specific class(es)/package(s) are covered with transaction logic introduced in aspect</li>
 *   <li>Test that all method in specific class(es)/package(s) has logging routine introduced in aspect</li>
 *   <li>Test that security Aspect is applied to specific classes, like XSS encoding, escaping, etc</li>
 * </ul>
 *
 * @version 1.0, 04/07/18
 * @since        04/07/18
 */
public class SimpleAspectWhiteBoxTest {

  @Mock private SimpleService        businessService;
  @Mock private NormalizationService normalizationService;


  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    normalizationService = mock(NormalizationService.class);
    SimpleAspect.getInstance().setNormalizationService(normalizationService);
  }

  @DisplayName("Normalization logic should be called implicitly for Business service result")
  @Test
  public void normalizationServiceShouldBeCalledOnBusinessServiceCall() {
    final String original = "889957a8-29ac-4cbd-9972-432152155f10";
    doAnswer(invocation -> original).when(businessService).returnTextAsIs(original);
    businessService.returnTextAsIs(original);
    verify(normalizationService).normalizeData(original);
  }

}
