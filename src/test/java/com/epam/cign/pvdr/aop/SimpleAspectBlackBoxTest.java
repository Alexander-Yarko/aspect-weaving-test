package com.epam.cign.pvdr.aop;

import com.epam.cign.pvdr.business.SimpleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.stream.Stream;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test that {@link SimpleService} behavior is changed to specific one.
 * It should be done with some AOP logic. No reference to Aspect here, it is a black box testing.
 *
 * @version 1.0, 04/07/18
 * @since        04/07/18
 */
public class SimpleAspectBlackBoxTest {

  @Mock private SimpleService businessService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  private static Stream<Arguments> originalAndNormalizedData() {
    return Stream.of(
        Arguments.of("abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        Arguments.of(" a  b  ", "A  B"),
        Arguments.of("0123456789", "0123456789"),
        Arguments.of(" 0 ", "0"));
  }

  @DisplayName("Business service should return normalized result")
  @ParameterizedTest(name = "{index} => original={0}, expected={1}")
  @MethodSource("originalAndNormalizedData")
  public void resultShouldBeNormalizedOnBusinessServiceCall(String original, String expected) {
    // the original result will be returned if no aspect is defined
    doAnswer(invocation -> original).when(businessService).returnTextAsIs(original);
    // actual result should be the normalized "original" one intercepted via aspect
    String actual = businessService.returnTextAsIs(original);
    // the expected result should
    assertEquals(expected, actual);
  }

}
